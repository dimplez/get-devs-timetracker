<?php

// Home View
Route::get('/', array( 'before' => 'auth', 'uses' => 'GetController@home' ));

// Login view
Route::get('/login', 'GetController@login');

// Logout Action
Route::get('/logout', 'GetController@logout');

// Export Action
Route::get('/export', array( 'before' => 'auth', 'uses' => 'GetController@export' ));

// URL for Ajax, Cut-off in timesheet page filter Action
Route::get('/changedate', array( 'before' => 'auth', 'uses' => 'GetController@changedate' ));

// Profile View
Route::get('/profile/{id}', 'GetController@profile');

// Settings View
Route::get('/settings', array( 'before' => 'auth', 'uses' => 'GetController@settings' ));

// Chart View
Route::get('/timesheet', array( 'before' => 'auth', 'uses' => 'GetController@timesheet' ));

// Save Log Time Action
Route::post('/savelog', array( 'before' => 'auth', 'uses' => 'PostController@savelog' ));

// Settings Action
Route::post('/updateinfo', array( 'before' => 'auth', 'uses' => 'PostController@updateinfo' ));

// Login Action
Route::post('/logging', 'PostController@login');

// Registration Action 
Route::post('/register', 'PostController@register');