<?php

class PostController extends BaseController {

	public function login() 
	{
		$data = Input::all();

		if ( Auth::attempt(array('username' => $data['login-username'], 'password' => $data['login-password']), true) ) {
		    return Redirect::intended('/');
		} else {
			Session::flash('failed', "User not recognised!");

			return Redirect::to('/login');
		}
	}

	public function savelog()
	{
		$_POST['date'] = date('Y-m-d');
		$_POST['time'] = date("g:i:s a");

		DB::table('logs')->insert(
	    	array(
	    		'id' 	=> Auth::id(),
	    		'type'	=> Input::get('type'),
	    		'time'	=> date( "g:i:s a", strtotime($_POST['time']) ),
	    		'date'	=> date( "Y-m-d", strtotime($_POST['date']) )
	    	)
	    );

	    Session::flash('success', 'Timelog Saved!');

		return Redirect::to('/');
	}

	public function updateinfo()
	{
		// if name is present
		if (Input::has('name')) {

			// find the user
			$user = User::find( Auth::id() );

			// fill some info
			$user->name = Input::get('name');
			$user->location = Input::get('location');
			$user->age = Input::get('age');
			$user->about = Input::get('about');

			// if it has a file uploaded
			if( Input::hasfile('avatar') ) {			
				// check if it already have an avatar
				$file = public_path() .'/assets/avatars/'. Auth::user()->avatar;

				// if avatar exists
				if ( File::exists($file) ) {
					// delete it
					File::delete($file);
				} 

				// update the database for the avatar name
				$user->avatar = 'tracker-'.Input::file('avatar')->getClientOriginalName();

			    // move the uploaded file
				Input::file('avatar')->move( public_path().'/assets/avatars/', 'tracker-'.Input::file('avatar')->getClientOriginalName() );
			}
			
			// save it
			$user->save();
		}

		return Redirect::to('/settings');
	}

	public function register()
	{
		// check if there already username in the DB
		$user = User::whereUsername( Input::get('reg-username') );

		// if it has
		if($user->count()) {
			// flash a message
			Session::flash('exist', "Username already exist!");
		} else {
			// else create the user
			User::create([
				'name'	 	=> Input::get('reg-name'),
	    		'username'	=> Input::get('reg-username'),
	    		'password'	=> Hash::make( Input::get('reg-password') ),
	    		'email' 	=> Input::get('reg-email'),
			]);
		}

		return Redirect::to('/login');
	}
}