<?php

class GetController extends BaseController {

	public function home()
	{
		/* Query the latest row in logs table
           of the current user */
        $latest = "Time-In";

        $latest_row = DB::table('logs')
            ->where('id', Auth::user()->id)
            ->orderBy('date', 'desc')
            ->orderBy('time', 'desc')
            ->first(); 

        if(count($latest_row) > 0) {
            switch ($latest_row->type) {
                case 'Time-In':
                    $latest = "Time-Out";
                    break;
                case 'Time-Out':
                    $latest = "Time-In";
                    break;
            }
        }

		return View::make('dashboard')->with('latest', $latest);
	}

	public function login() 
	{
		return View::make('login');
	}

	public function logout()
	{
	    Auth::logout();
		Session::flush();

		Session::flash('logout', "You are successfully logged out.");

	    return Redirect::to('/login');
	}

	public function timesheet()
	{
		return View::make('timesheet');
	}

	public function export()
	{
		// return View::make('excelView');

		// Create an Excel File named invoice
		Excel::create('invoice', function($excel) {

			//add a sheet named sheet
			$excel->sheet('getdevs', function($sheet) {

				// load the template
				$sheet->loadView('excelView');
			});
		})-> export('xls');

		return Redirect::to('chart');
	}

	public function changedate()
	{
		$startdate = $_GET['startdate']; 
		$enddate = $_GET['enddate'];

		$changed = DB::table('users')
					->join('logs', 'users.id', '=', 'logs.id')
					->select('users.name', 'logs.type', 'logs.date', 'logs.time')
		            ->whereBetween('date', array( $startdate, $enddate ))
					->orderBy('date', 'desc')
		            ->orderBy('time', 'desc')
					->get();

		return $changed;
	}

	public function profile($id)
	{
		$data = array( 'id' => $id );

	    return View::make('profile', $data);
	}

	public function settings()
	{
		return View::make('settings');
	}
}