<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Get Devs :: {{ Request::segment(1) ? ucwords(Request::segment(1)) : 'Timetracker' }}</title>

		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css') }}" />
		

		<link rel="stylesheet" href="{{ URL::asset('assets/css/font-awesome.min.css') }}" />

		<!-- page specific plugin styles -->
		@yield('header')

		<!-- text fonts -->
		<link rel="stylesheet" href="{{ URL::asset('assets/css/open-sans.css') }}" />

		<!-- ace styles -->
		<link rel="stylesheet" href="{{ URL::asset('assets/css/ace.min.css') }}" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="{{ URL::asset('assets/css/ace-part2.min.css') }}" />
		<![endif]-->
		<link rel="stylesheet" href="{{ URL::asset('assets/css/ace-skins.min.css') }}" />
		<link rel="stylesheet" href="{{ URL::asset('assets/css/ace-rtl.min.css') }}" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="{{ URL::asset('assets/css/ace-ie.min.css') }}" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="{{ URL::asset('assets/js/ace-extra.min.js') }}"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
			<script src="{{ URL::asset('assets/js/html5shiv.min.js') }}"></script>
			<script src="{{ URL::asset('assets/js/respond.min.js') }}"></script>
		<![endif]-->
		<link rel="stylesheet" href="{{ URL::asset('assets/css/custom.css') }}" />

	</head>

	<body class="skin-1">
		<div id="navbar" class="navbar navbar-default">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>

			<div class="navbar-container" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<div class="navbar-header pull-left">
					<a href="/" class="navbar-brand">
						<small>
							<i class="fa fa-clock-o"></i>
							TimeTracker
						</small>
					</a>
				</div>

				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								
								@if (Auth::check()) 
									@if (Auth::user()->avatar)
										<img class="nav-user-photo" src="{{ URL::asset('assets/avatars/'. Auth::user()->avatar) }}" />
									@else 
										<img class="nav-user-photo" src="{{ URL::asset('assets/avatars/user.jpg') }}" />
									@endif
								<span class="user-info">
									<small>Welcome,</small>
									{{ ucwords(Auth::user()->name) }}
								</span>
								<i class="ace-icon fa fa-caret-down"></i>
								@else 
									@if ($user->avatar)
										<img class="nav-user-photo" src="{{ URL::asset('assets/avatars/'.$user->avatar) }}" />
									@else 
										<img class="nav-user-photo" src="{{ URL::asset('assets/avatars/user.jpg') }}" />
									@endif
								<span class="user-info">
									<small>Welcome,</small>
									Guest
								</span>
								@endif
							</a>
							{{--  if the user if logged-in, show this menu --}}
							@if (Auth::check()) 
							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="/settings">
										<i class="ace-icon fa fa-cog"></i>
										Settings
									</a>
								</li>

								<li>
									<a href="/profile/{{{ Auth::user()->id }}}">
										<i class="ace-icon fa fa-user"></i>
										Profile
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="/logout">
										<i class="ace-icon fa fa-power-off"></i>
										Logout
									</a>
								</li>
							</ul>
							@endif
						</li>
					</ul>
				</div>
			</div><!-- /.navbar-container -->
		</div>
		<div class="main-container" id="main-container">