			<div class="footer">
				<div class="footer-inner">
					<div classs="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">Get</span>
							Devs &copy; 2013-2014
						</span>

						&nbsp; &nbsp;
						<span class="action-buttons">
							<a href="#">
								<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
							</a>
						</span>
					</div>
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
			<script src="{{ URL::asset('assets/js/jquery.min.2.1.1.js') }}"></script>
		<!-- <![endif]-->

		<!--[if IE]>
			<script src="{{ URL::asset('assets/js/jquery.min.1.11.1.js') }}"></script>
		<![endif]-->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='{{ URL::asset('assets/js/jquery.min.js') }}'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
			<script type="text/javascript">
				window.jQuery || document.write("<script src='{{ URL::asset('assets/js/jquery1x.min.js') }}'>"+"<"+"/script>");
			</script>
		<![endif]-->

		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='{{ URL::asset('assets/js/jquery.mobile.custom.min.js') }}'>"+"<"+"/script>");
		</script>
		<script src="{{ URL::asset('assets/js/bootstrap.min.3.2.0.js') }}"></script>

		<!-- ace scripts -->
		<script src="{{ URL::asset('assets/js/ace-elements.min.js') }}"></script>
		<script src="{{ URL::asset('assets/js/ace.min.js') }}"></script>

		<!-- page specific plugin scripts inline scripts related to this page -->
		@yield('footer')
		
	</body>
</html>