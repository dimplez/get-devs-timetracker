<div id="sidebar" class="sidebar responsive sidebar-scroll {{{ Auth::check() ? '' : 'hide' }}}">
	<?php
		$cur_id = '';
		// if the viewer is logged-in
		if( Auth::check() ) {
			// assign its id to the var
			$cur_id = Auth::user()->id;
		}
	?> 	
	<ul class="nav nav-list">
		<li class="highlight hover <?php echo Request::segment(1) == '' ? 'active' : ''; ?>">
			<a href="/">
				<i class="menu-icon fa fa-home"></i>
				<span class="menu-text"> Time Log </span>
			</a>
			<b class="arrow"></b>
		</li>

		<li class="highlight hover <?php echo Request::segment(1) == 'timesheet' ? 'active': ''; ?>">
			<a href="/timesheet">
				<i class="menu-icon glyphicon glyphicon-list-alt"></i>
				<span class="menu-text"> Timesheet </span>
			</a>
			<b class="arrow"></b>
		</li>

		<li class="highlight hover <?php echo Request::segment(1) == 'profile' ? 'active': ''; ?>">
			<a href="/profile/{{ $cur_id }}">
				<i class="menu-icon fa fa-user"></i>
				<span class="menu-text"> Profile </span>
			</a>
			<b class="arrow"></b>
		</li>

		<li class="highlight hover <?php echo Request::segment(1) == 'settings' ? 'active': ''; ?>">
			<a href="/settings">
				<i class="menu-icon fa fa-cog"></i>
				<span class="menu-text"> Settings </span>
			</a>
			<b class="arrow"></b>
		</li>
	</ul><!-- /.nav-list -->

	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
	</div>
</div>