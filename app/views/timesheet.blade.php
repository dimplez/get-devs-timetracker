@extends('layouts.layout')

@section('header')
	{{-- header specific plugin styles--}}

    <link rel="stylesheet" href="{{ URL::asset('assets/css/daterangepicker.css') }}" />
@stop

@section('content')
    <div class="well">
        <h4 class="green smaller lighter">
            HUMAN RESOURCES AND TIMEKEEPING SUMMARY SHEET
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
               September 2014
            </small>
        </h4>

        <dl class="dl-horizontal">
            <dt>COMPANY NAME: </dt>
            <dd>Get Devs Asia, Inc.</dd>
            
            <dt>TIMEKEEPING CUT OFF: </dt>
            <dd>September 21 - October 5, 2014</dd>
        </dl>
        <hr />
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-sm-offset-9">
                <label for="range">Cut-off Range</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar bigger-110"></i>
                    </span>

                    <input class="form-control" type="text" name="range" id="range" />
                </div>
            </div>
        </div>
    </div>
	<table class="timesheet table table-striped table-bordered table-hover">
        <thead>
            <tr>
            	<th>
                    <i class="ace-icon fa fa-user bigger-110"></i>
                    Name
                </th>
                <th>
                    <i class="ace-icon fa fa-exchange bigger-110"></i>
                    Type
                </th>
                <th>
                    <i class="ace-icon fa fa-calendar bigger-110"></i>
                    Date
                </th>
                <th>
                    <i class="ace-icon fa fa-clock-o bigger-110"></i>
                    Time
                </th>
            </tr>
        </thead>
        <tbody>
        	<?php
        		$all = DB::table('users')
        			->join('logs', 'users.id', '=', 'logs.id')
        			->select('users.name', 'logs.type', 'logs.date', 'logs.time')
                    ->whereBetween('date', array( date('Y-m-01'), date('Y-m-31') ))
        			->orderBy('date', 'desc')
                    ->orderBy('time', 'desc')
        			->get();

        		foreach ($all as $single) {
        			$label = $single->type == 'Time-In' ? 'success arrowed-right' : 'danger arrowed';
        			
        			echo '<tr>';
        			echo '<td>'. $single->name .'</td>';
	                echo '<td><span class="label label-sm label-'. $label .'">'. $single->type .'</span></td>';
	                echo '<td>'. date( "F d, Y", strtotime( $single->date ) ) .'</td>';
	                echo '<td>'. date( "g:i a", strtotime( $single->time ) ) .'</td>';
	                echo '</tr>';
        		}
        	?>
        </tbody>
    </table>

    <a href="export" class="btn btn-primary">Export</a>
@stop

@section('footer')
	{{-- page specific plugin scripts inline scripts related to this page --}}
    
    <script src="{{ URL::asset('assets/js/date-time/moment.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/date-time/daterangepicker.min.js') }}"></script>

    <script type="text/javascript">
        jQuery(function($) {
            var d = new Date();
            $('input[name=range]').daterangepicker({
                format: 'YYYY-MM-DD',
                startDate: d.getFullYear() + '-' + ( d.getMonth() + 1 ) + '-' + '5',
                endDate: d.getFullYear() + '-' + ( d.getMonth() + 2 ) + '-' + '20',
                separator: ' / ',
                'applyClass' : 'btn-sm btn-success',
                'cancelClass' : 'btn-sm btn-default',
                locale: {
                    applyLabel: 'Apply',
                    cancelLabel: 'Cancel',
                }
            }, function(start, end, label) {
                // Ajax to load the selected dates
                $.ajax({
                    url: '/changedate',
                    type: 'GET',
                    data: { 
                        'startdate' : start.format('YYYY-MM-DD'), 
                        'enddate'   : end.format('YYYY-MM-DD') 
                    },
                    success: function(response) {
                        var result = '';
                        var label = '';
                        
                        // loop though each result
                        $.each(response, function(index, value) {
                            label = value.type == 'Time-In' ? 'success arrowed-right' : 'danger arrowed';
                            // put it in the result var
                            result += '<tr><td>'+ value.name +'</td>';
                            result += '<td><span class="label label-sm label-'+ label +'">'+ value.type +'</span></td>';
                            result += '<td>'+ moment(value.date).format('MMMM DD, YYYY') +'</td>';
                            result += '<td>'+ value.time +'</td></tr>';
                        });
                        // change the table content
                        $('table.timesheet tbody').html(result);
                    },
                    error: function(error) { console.log(error); }
                });

            })
            .prev().on(ace.click_event, function(){
                $(this).next().focus();
            });
        });
    </script>
@stop