@extends('layouts.layout')

@section('header')
    {{-- header specific plugin styles--}}

    <link rel="stylesheet" href="{{ URL::asset('assets/css/jquery.gritter.css') }}" />
@stop

@section('content')
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="widget-box">
                <div class="widget-header text-center">
                    <h4 class="widget-title">Time Form</h4>
                </div>

                <div class="widget-body">
                    <div class="widget-main no-padding">
                        {{ Form::open(array('url' => '/savelog', 'id' => 'savelog')) }}
                            <fieldset>
                                <div class="form-group logtype-cont">
                                    {{ Form::select('dummy',
                                    array( 'Time-In' => 'Time-In', 'Time-Out' => 'Time-Out' ), $latest, 
                                    array( 'id' => 'log_type', 'class'=>'form-control', 'disabled' => 'disabled')) }}

                                    {{ Form::hidden('type', $latest) }}
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        {{ Form::text('date', $value = NULL,
                                        array( 'class' => 'form-control date', 'readonly' => 'readonly', 'placeholder' => date('Y-m-d') )) }}

                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar bigger-110"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group bootstrap-timepicker">
                                        {{ Form::text('time', $value = NULL, 
                                        array( 'class' => 'form-control', 'readonly' => 'readonly', 'placeholder' => date('g:i:s a') )) }}

                                        <span class="input-group-addon">
                                            <i class="fa fa-clock-o bigger-110"></i>
                                        </span>
                                    </div>
                                </div>
                            </fieldset>

                            <div class="form-actions center">
                                <button type="submit" class="btn btn-sm btn-primary savelog-btn">
                                    <i class="ace-icon fa fa-save icon-on-right bigger-110"></i> 
                                    Submit
                                </button>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hr dotted"></div>

    <table id="log-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>
                    <i class="ace-icon fa fa-exchange bigger-110"></i>
                    Type
                </th>
                <th>
                    <i class="ace-icon fa fa-calendar bigger-110"></i>
                    Date
                </th>
                <th>
                    <i class="ace-icon fa fa-clock-o bigger-110"></i>
                    Time
                </th>
            </tr>
        </thead>
        <tbody>
        <?php 
            // Get all rows in logs db where id is the current user
            $logs = DB::table('logs')
                    ->where('id', Auth::id() )
                    ->orderBy('date', 'desc')
                    ->orderBy('time', 'desc')
                    ->get(); 

            foreach ($logs as $log) {
                $label = $log->type == 'Time-In' ? 'success arrowed-right' : 'danger arrowed';
                
                echo '<tr>';
                echo '<td><span class="label label-sm label-'. $label .'">'. $log->type .'</span></td>';
                echo '<td>'. date( "F d, Y", strtotime( $log->date ) ) .'</td>';
                echo '<td>'. date( "g:i a", strtotime( $log->time ) ) .'</td>';
                echo '</tr>';
            }
        ?>
        </tbody>
    </table>
@stop

@section('footer')
    {{-- page specific plugin scripts inline scripts related to this page --}}
    
    <script src="{{ URL::asset('assets/js/jquery.gritter.min.js') }}"></script>

    <script type="text/javascript">
        jQuery(function($) {
            // Show gritter after save log
            @if (Session::has('success'))
                $.gritter.add({
                    title: 'Time Log Saved!',
                    text: 'Your Time table is updated.',
                    class_name: 'gritter-success'
                });
            @endif
        });
    </script>
@stop