@extends('layouts.layout')

@section('header')
	{{-- header specific plugin styles--}}

	<link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap-editable.css') }}" />
@stop

@section('content')
	<div class="update-page">
		<div class="page-header">
			<h1>
				Update Info
				{{-- <small>
					<i class="ace-icon fa fa-angle-double-right"></i>
					Common form elements and layouts
				</small> --}}
			</h1>
		</div><!-- /.page-header -->
		{{ Form::open(array('url' => '/updateinfo', 'id' => 'updateinfo', 'class' => 'form-horizontal', 'role' => 'form', 'files' => true)) }}
			<div class="row">
				<div class="col-xs-0 col-sm-3">
					@if (Auth::user()->avatar)
						<img src="{{ URL::asset('assets/avatars/'. Auth::user()->avatar) }}" class="avatar" />
					@endif
					<div class="space-10"></div>
				</div>
				<div class="col-xs-10 col-sm-5">
					{{ Form::file('avatar', array('class' => 'file-input')) }}
				</div>
				<div class="col-xs-0 col-sm-4">&nbsp;</div>
			</div>
			
			<div class="space-10"></div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Name </label>

				<div class="col-sm-9">
					{{ Form::text('name', $value = Auth::user()->name, array( 'class' => 'col-xs-10 col-sm-7', 'placeholder' => 'Name', 'required' => 'required' )) }}
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Location </label>

				<div class="col-sm-9">
					{{ Form::text('location', $value = Auth::user()->location, array( 'class' => 'col-xs-10 col-sm-7', 'placeholder' => 'Location' )) }}
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Age </label>

				<div class="col-sm-9">
					{{ Form::text('age', $value = Auth::user()->age, array( 'class' => 'col-xs-10 col-sm-7', 'placeholder' => 'Age' )) }}
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Joined </label>

				<div class="col-sm-9">
					<div class="well well-sm col-xs-10 col-sm-7">
						{{ Auth::user()->created_at->format('Y-m-d') }}
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Last Online </label>

				<div class="col-sm-9">
					<div class="well well-sm col-xs-10 col-sm-7">
						{{ Auth::user()->updated_at->format('g:i a') }}
					</div>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> About Me </label>

				<div class="col-sm-9">
					<div class="widget-box widget-color-blue col-xs-10 col-sm-7">
					   <div class="widget-header widget-header-small"></div>
					   <div class="widget-body">
					      <div class="widget-main no-padding">
					         <textarea name="about" data-provide="markdown" rows="10">{{ Auth::user()->about }}</textarea>
					      </div>
					   </div>
					</div>
				</div>
			</div>

			<div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<button class="btn btn-info" type="submit">
						<i class="ace-icon fa fa-check bigger-110"></i>
						Submit
					</button>

					&nbsp; &nbsp; &nbsp;
					<button class="btn" type="reset">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						Reset
					</button>
				</div>
			</div>
		{{ Form::close() }}
	</div>
@stop

@section('footer')
	{{-- page specific plugin scripts inline scripts related to this page --}}
	<script src="{{ URL::asset('assets/js/markdown/markdown.min.js') }}"></script>
	<script src="{{ URL::asset('assets/js/markdown/bootstrap-markdown.min.js') }}"></script>
	<script src="{{ URL::asset('assets/js/bootbox.min.js') }}"></script>

	<script type="text/javascript">
		jQuery(function($) {
			$('.update-page')
			.find('input[type=file]').ace_file_input({
				style:'well',
				btn_choose:'Change avatar',
				btn_change:null,
				no_icon:'ace-icon fa fa-picture-o',
				thumbnail:'fit',
				droppable:true,
				
				allowExt: ['jpg', 'jpeg', 'png', 'gif', 'bmp'],
				allowMime: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif', 'image/bmp']
			})
			.end().find('button[type=reset]').on(ace.click_event, function(){
				$('.update-page input[type=file]').ace_file_input('reset_input');
			});
		});

	</script>
@stop