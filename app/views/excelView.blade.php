<html>
	<head>
		<link rel="stylesheet" href="{{ URL::asset('assets/css/custom.css') }}" />
	</head>
	<body class="export">
		<div class="well">
	        <h4 class="green smaller lighter">
	            HUMAN RESOURCES AND TIMEKEEPING SUMMARY SHEET
	            <small>
	                <i class="ace-icon fa fa-angle-double-right"></i>
	               September 2014
	            </small>
	        </h4>

	        <dl class="dl-horizontal">
	            <dt>COMPANY NAME: </dt>
	            <dd>Get Devs Asia, Inc.</dd>
	            
	            <dt>TIMEKEEPING CUT OFF: </dt>
	            <dd>September 21 - October 5, 2014</dd>
	        </dl>
	    </div>
		<table class="table table-striped table-bordered table-hover">
	        <thead>
	            <tr>
	            	<th>Name</th>
	                <th>Type</th>
	              	<th>Date</th>
	                <th>Time</th>
	            </tr>
	        </thead>
	        <tbody>
	        	<?php
	        		$all = DB::table('users')
	        			->join('logs', 'users.id', '=', 'logs.id')
	        			->select('users.name', 'logs.type', 'logs.date', 'logs.time')
	        			->orderBy('date', 'desc')
	        			->get();

	        		foreach ($all as $single) {
	        			$label = $single->type == 'Time-In' ? 'success arrowed-right' : 'danger arrowed';
	        			
	        			echo '<tr>';
	        			echo '<td>'. $single->name .'</td>';
		                echo '<td><span class="label label-sm label-'. $label .'">'. $single->type .'</span></td>';
		                echo '<td>'. date( "F d, Y", strtotime( $single->date ) ) .'</td>';
		                echo '<td>'. date( "g:i a", strtotime( $single->time ) ) .'</td>';
		                echo '</tr>';
	        		}
	        	?>
	        </tbody>
	    </table>
	</body>
</html>