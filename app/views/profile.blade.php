@extends('layouts.layout')

@section('header')
	{{-- header specific plugin styles--}}

@stop

@section('content')
	<?php 
		// get the user id based on the URL
		$user = User::find($id); 
	?>

	<div>
		<div id="user-profile-1" class="user-profile row">
			<div class="col-xs-12 col-sm-3 center">
				<div>
					@if ($user->avatar)
						<img id="avatar" class="editable img-responsive avatar" alt="{{ $user->name }}'s Avatar" src="{{ URL::asset('assets/avatars/'. $user->avatar) }}" />
					@else
						<span class="profile-picture">
							<img id="avatar" class="editable img-responsive" alt="{{ $user->name }}'s Avatar" src="{{ URL::asset('assets/avatars/profile-pic.jpg') }}" />
						</span>
					@endif

					<div class="space-4"></div>

					<div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
						<div class="inline position-relative">
							<a href="profile.html#" class="user-title-label dropdown-toggle" data-toggle="dropdown">
								<i class="ace-icon fa fa-circle light-green"></i>
								&nbsp;
								<span class="white">{{ $user->name }}</span>
							</a>

							@if (Auth::check()) 
							<ul class="align-left dropdown-menu dropdown-caret dropdown-lighter">
								<li class="dropdown-header"> Change Status </li>

								<li>
									<a href="profile.html#">
										<i class="ace-icon fa fa-circle green"></i>
										&nbsp;
										<span class="green">Available</span>
									</a>
								</li>

								<li>
									<a href="profile.html#">
										<i class="ace-icon fa fa-circle red"></i>
										&nbsp;
										<span class="red">Busy</span>
									</a>
								</li>

								<li>
									<a href="profile.html#">
										<i class="ace-icon fa fa-circle grey"></i>
										&nbsp;
										<span class="grey">Invisible</span>
									</a>
								</li>
							</ul>
							@endif
						</div>
					</div>
				</div>

				<div class="space-6"></div>

				<div class="profile-contact-info">
					<div class="profile-contact-links align-left">
						<a href="mailto:{{ $user->email }}" class="btn btn-link">
							<i class="ace-icon fa fa-envelope bigger-120 pink"></i>
							Send a message
						</a>
					</div>

					<div class="space-6"></div>

					{{-- <div class="profile-social-links align-center">
						<a href="profile.html#" class="tooltip-info" title="" data-original-title="Visit my Facebook">
							<i class="middle ace-icon fa fa-facebook-square fa-2x blue"></i>
						</a>

						<a href="profile.html#" class="tooltip-info" title="" data-original-title="Visit my Twitter">
							<i class="middle ace-icon fa fa-twitter-square fa-2x light-blue"></i>
						</a>

						<a href="profile.html#" class="tooltip-error" title="" data-original-title="Visit my Pinterest">
							<i class="middle ace-icon fa fa-pinterest-square fa-2x red"></i>
						</a>
					</div> --}}
				</div>
			</div>

			<div class="col-xs-12 col-sm-9">

				<div class="profile-user-info profile-user-info-striped">
					<div class="profile-info-row">
						<div class="profile-info-name"> Name </div>

						<div class="profile-info-value">
							<span class="editable" id="username">{{ $user->name }}</span>
						</div>
					</div>

					<div class="profile-info-row">
						<div class="profile-info-name"> Location </div>

						<div class="profile-info-value">
							<i class="fa fa-map-marker light-orange bigger-110"></i>
							<span class="editable" id="country">{{ $user->location }}</span>
						</div>
					</div>

					<div class="profile-info-row">
						<div class="profile-info-name"> Age </div>

						<div class="profile-info-value">
							<span class="editable" id="age">{{ $user->age }}</span>
						</div>
					</div>

					<div class="profile-info-row">
						<div class="profile-info-name"> Joined </div>

						<div class="profile-info-value">
							<span class="editable" id="signup">{{ $user->created_at->format('Y-m-d') }}</span>
						</div>
					</div>

					<div class="profile-info-row">
						<div class="profile-info-name"> Last Online </div>

						<div class="profile-info-value">
							<span class="editable" id="login">{{ $user->updated_at->format('g:i a') }}</span>
						</div>
					</div>

					<div class="profile-info-row">
						<div class="profile-info-name"> About Me </div>

						<div class="profile-info-value">
							<span class="editable" id="about">{{ $user->about }}</span>
						</div>
					</div>
				</div>

				<div class="space-20"></div>

				<div class="widget-box transparent">
					<div class="widget-header widget-header-small">
						<h4 class="widget-title blue smaller">
							<i class="ace-icon fa fa-rss orange"></i>
							Recent Activities
						</h4>
					</div>

					<div class="widget-body">
						<div class="widget-main padding-8">
							<div id="profile-feed-1" class="profile-feed">
							<?php 
					            // Get all rows in logs db where id is the current user
					            $logs = DB::table('logs')
					                    ->where('id', $user->id )
					                    ->orderBy('time', 'desc')
					                    ->take(10)
					                    ->get(); 

					            foreach ($logs as $log) : ?>
								<div class="profile-activity clearfix">
									<div>
							            @if ($user->avatar)
											<img id="avatar" class="pull-left img-responsive" alt="{{ $user->name }}'s Avatar" src="{{ URL::asset('assets/avatars/'. $user->avatar) }}" />
										@else
											<img class="pull-left" alt="{{ $user->name }}'s avatar" src="{{ URL::asset('assets/avatars/avatar5.png') }}" />
										@endif
										<a class="user" href="profile/{{ $user->id }}"> {{ $user->name }} </a>
										just {{ $log->type }}

										<div class="time">
											<i class="ace-icon fa fa-clock-o bigger-110"></i>
											{{ date( "F d, Y", strtotime( $log->date ) ) .', '. date( "g:i a", strtotime( $log->time ) ) }}
										</div>
									</div>
								</div>
					        <?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('footer')
	{{-- page specific plugin scripts inline scripts related to this page --}}

@stop